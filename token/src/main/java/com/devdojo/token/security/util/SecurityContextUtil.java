package com.devdojo.token.security.util;

import com.devdojo.core.model.ApplicationUser;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Slf4j
public class SecurityContextUtil {
    private SecurityContextUtil() {
    }

    public static void setSecurityContext(SignedJWT signedJWT) {
        try {
            JWTClaimsSet jwtClaimsSet = signedJWT.getJWTClaimsSet();

            String usuario = jwtClaimsSet.getSubject();

            if (isNull(usuario)) {
                throw new JOSEException("Usuário ausente no JWT");
            }

            List<String> authorities = jwtClaimsSet.getStringListClaim("authorities");

            ApplicationUser applicationUser = ApplicationUser
                    .builder()
                    .id(jwtClaimsSet.getLongClaim("userId"))
                    .role(String.join(",", authorities))
                    .build();

            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(applicationUser, null, criarAuthorities(authorities));

            auth.setDetails(signedJWT.serialize());

            SecurityContextHolder.getContext().setAuthentication(auth);

        } catch (Exception exception) {
            log.error("Error ao prencher security contex", exception);
            SecurityContextHolder.clearContext();
        }
    }

    private static List<SimpleGrantedAuthority> criarAuthorities(List<String> authorities) {
        return authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }
}
