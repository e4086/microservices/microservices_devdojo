package com.devdojo.token.security.creator;

import com.devdojo.core.model.ApplicationUser;
import com.devdojo.core.property.JwtConfiguration;
import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPublicKey;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TokenCreator {

    private final JwtConfiguration jwtConfiguration;

    @SneakyThrows
    public SignedJWT criarSignedJWT(Authentication auth) {
        log.info("Iniciando criação do signed JWT");

        JWTClaimsSet jwtClaimsSet = criarJWTClaimsSet(auth);

        KeyPair rsaKeys = gerarKeyPair();

        log.info("Construindo JWK das chaves RSA");

        RSAKey jwk = new RSAKey.Builder((RSAPublicKey) rsaKeys.getPublic()).keyID(UUID.randomUUID().toString()).build();

        SignedJWT signedJWT = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.RS256)
                .jwk(jwk)
                .type(JOSEObjectType.JWT)
                .build(), jwtClaimsSet);

        log.info("Assinando token com chave privada RSA");

        RSASSASigner signer = new RSASSASigner(rsaKeys.getPrivate());

        signedJWT.sign(signer);

        log.info("Token: {}   " + signedJWT.serialize());

        return signedJWT;
    }

    private JWTClaimsSet criarJWTClaimsSet(Authentication auth) {
        ApplicationUser applicationUser = (ApplicationUser) auth.getPrincipal();

        log.info("Criando Objeto JWTClaimsSet para {}", applicationUser);

        return new JWTClaimsSet.Builder()
                .subject(applicationUser.getUsername())
                .claim("authorities", formatarAuthotities(auth))
                .claim("userId", applicationUser.getId())
                .issuer("http://academy.devdojo")
                .issueTime(new Date())
                .expirationTime(new Date(System.currentTimeMillis() + (jwtConfiguration.getExpiration() * 1000L)))
                .build();
    }

    @SneakyThrows
    private KeyPair gerarKeyPair() {
        log.info("Gerando chaves RSA 2048  bits");

        KeyPairGenerator gernarator = KeyPairGenerator.getInstance("RSA");

        gernarator.initialize(2048);

        return gernarator.genKeyPair();
    }

    public String criptografarToken(SignedJWT signedJWT) throws JOSEException {
        log.info("Iniciando criptografia do token ");

        DirectEncrypter directEncrypter = new DirectEncrypter(jwtConfiguration.getPrivateKey().getBytes());

        JWEObject jweObject = new JWEObject(new JWEHeader.Builder(JWEAlgorithm.DIR, EncryptionMethod.A128CBC_HS256)
                .contentType("JWT")
                .build(), new Payload(signedJWT));

        log.info("Criptografando token com chave privada do sistema.");

        jweObject.encrypt(directEncrypter);

        log.info("Token criptografado.");

        return jweObject.serialize();
    }

    private List<String> formatarAuthotities(Authentication authentication) {
        return authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
    }
}
