package com.devdojo.token.security.converter;

import com.devdojo.core.property.JwtConfiguration;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.SignedJWT;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TokenConverter {

    private final JwtConfiguration jwtConfiguration;

    @SneakyThrows
    public String descriptografar(String tokenCriptografado) {
        log.info("Descriptografando token");

        JWEObject jweObject = JWEObject.parse(tokenCriptografado);

        DirectDecrypter directDecrypter = new DirectDecrypter(jwtConfiguration.getPrivateKey().getBytes());

        jweObject.decrypt(directDecrypter);

        log.info("Token Descriptografado, retornando signed token...");

        return jweObject.getPayload().toSignedJWT().serialize();
    }


    @SneakyThrows
    public void validarAssinaturaToken(String signedToken) {
        log.info("Iniciando método para validar assinatura do token...");

        SignedJWT signedJWT = SignedJWT.parse(signedToken);

        log.info("Token analisado! Recuperando public key para signed token.");

        RSAKey publicKey = RSAKey.parse(signedJWT.getHeader().getJWK().toJSONObject());

        log.info("Public key recuperada, validando assinatura");

        if (!signedJWT.verify(new RSASSAVerifier(publicKey))) {
            throw new AccessDeniedException("Assinatura de toke inválida!");
        }

        log.info("Assinatura de toke valida.");
    }
}
