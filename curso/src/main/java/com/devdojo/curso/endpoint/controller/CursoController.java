package com.devdojo.curso.endpoint.controller;

import com.devdojo.core.model.Curso;
import com.devdojo.curso.endpoint.service.CursoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("/v1/admin/curso")
@Api("Endpoint de Cursos")
public class CursoController {

    private final CursoService cursoService;

    @ApiOperation(value = "Listagem de Cursos", response = Curso[].class)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Iterable<Curso>> buscarTodos(Pageable pageable) {
        return new ResponseEntity<>(cursoService.buscarTodos(pageable), HttpStatus.OK);
    }
}
