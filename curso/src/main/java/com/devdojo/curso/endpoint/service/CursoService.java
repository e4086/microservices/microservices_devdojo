package com.devdojo.curso.endpoint.service;

import com.devdojo.core.model.Curso;
import com.devdojo.core.repository.CursoRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CursoService {

    private final CursoRepository cursoRepository;

    public Iterable<Curso> buscarTodos(Pageable pageable) {
        log.info("Listando Cursos");
        return cursoRepository.findAll(pageable);
    }
}
