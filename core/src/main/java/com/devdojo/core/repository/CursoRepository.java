package com.devdojo.core.repository;

import com.devdojo.core.model.Curso;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CursoRepository extends PagingAndSortingRepository<Curso, Long> {
}
