# microservices_devdojo

Implementação de código assistindo os videoaulas do curso do canal DevDojo para criar os micro serviços abaixo:
 Discovery, Gateway, Autenticação e Cadastro de Cursos

Link playlist: https://www.youtube.com/playlist?list=PL62G310vn6nH_iMQoPMhIlK_ey1npyUUl

## Passo 1 - Subir container docker banco de dados (mysql)

`docker-compose uo -d`

## Passo 2 - Criar banco de dados 

Acesse o console para executar comandos no banco de dados via alguma ferramenta que permita conectar ao banco de dados criado.

Usuário: root
Senha: devdojo

Obs. Caso queira mudar a senha ou usuário mudar também em cada um dos arquivos application.yml dos módulos para que a aplicação funcione corretamente.

Execute o comando para criação do banco de dados usado nas APIs:

`create database devdojo`

## Ordem para subir os serviços das APIS

- Discovery
- Gateway
- Auth
- Curso

Obs. Os demais módulos core e token não possui APIs apenas são organizados para separação melhor do código e evitar duplicação de código visto que são usados nos demais módulos citados acima que possui APIs.

## Acessar a documentação com Swagger
API de Autenticação:

http://localhost:8080/gateway/auth/swagger-ui.html

API de Cadastro de Curso:
http://localhost:8080/gateway/auth/swagger-ui.html

